﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace fenykepalbum_4_3.Models
{
    public class Fileok
    {     
        public List<string> fnevek { get;set; }
        public Fileok(string útvonal)
        {
            fnevek = Directory.EnumerateFiles(útvonal).Select(x => x.Substring(útvonal.Length)).ToList();       
        }

    }
}
